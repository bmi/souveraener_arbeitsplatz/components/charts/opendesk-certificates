<!--
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# opendesk-certificates

A Helm chart for getting certificates for openDesk

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository
```console
helm repo add opendesk-certificates https://gitlab.opencode.de/api/v4/projects/1376/packages/helm/stable
helm install my-release --version 3.1.1 opendesk-certificates/opendesk-certificates
```

### Install via OCI Registry
```console
helm repo add opendesk-certificates oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-certificates
helm install my-release --version 3.1.1 opendesk-certificates/opendesk-certificates
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| oci://registry.opencode.de/bmi/opendesk/components/external/charts/bitnami-charts | common | 2.14.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| cleanup.keepRessourceOnDelete | bool | `true` | Keep certificate on delete of this release. |
| global.domain | string | `"example.tld"` | The Top-Level-Domain (TLD) name which is used in f.e. in Ingress component. |
| global.hosts.openxchange | string | `"webmail"` |  |
| issuerRef.kind | string | `"ClusterIssuer"` | Type of Issuer, f.e. "Issuer" or "ClusterIssuer". |
| issuerRef.name | string | `"letsencrypt-live"` | Name of cert-manager.io Issuer resource. |
| selfSigned.caCertificate.create | bool | `true` | Create a new root CA certificate. |
| selfSigned.caCertificate.secretName | string | `""` | Alternatively specify kubernetes secret which is used as root ca. |
| selfSigned.enabled | bool | `false` | Enable self-signed certificate generation. |
| selfSigned.keystores.jks.enabled | bool | `true` | Enable java keystore/truststore creation. |
| selfSigned.keystores.jks.password.secret.key | string | `"password"` | Key in secret containing the password. |
| selfSigned.keystores.jks.password.secret.name | string | `""` | Secret name containing password (higher precedence than plain value). |
| selfSigned.keystores.jks.password.value | string | `""` | Password as plain value. |
| selfSigned.organizationalUnits | list | `["Datacenter Operations"]` | organizationalUnits used in the self-signed certificate resource. |
| selfSigned.organizations | list | `["European Company that Makes Everything (ECME) Inc."]` | Organization used in the self-signed certificate resource. |
| selfSigned.privateKey.algorithm | string | `"ECDSA"` | Private key algorithm. |
| selfSigned.privateKey.size | int | `256` | Privat key length |
| wildcard | bool | `false` | When true a wildcard certificate will be requested instead of a single certificate with additional names. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
